/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package com.erpgs.fiscalprint.agrosilos.model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import org.compiere.model.*;
import org.compiere.util.KeyNamePair;

/** Generated Interface for C_FiscalPrintConf
 *  @author iDempiere (generated) 
 *  @version Release 5.1
 */
@SuppressWarnings("all")
public interface I_C_FiscalPrintConf 
{

    /** TableName=C_FiscalPrintConf */
    public static final String Table_Name = "C_FiscalPrintConf";

    /** AD_Table_ID=1000000 */
    public static final int Table_ID = MTable.getTable_ID(Table_Name);

    KeyNamePair Model = new KeyNamePair(Table_ID, Table_Name);

    /** AccessLevel = 7 - System - Client - Org 
     */
    BigDecimal accessLevel = BigDecimal.valueOf(7);

    /** Load Meta Data */

    /** Column name AD_Client_ID */
    public static final String COLUMNNAME_AD_Client_ID = "AD_Client_ID";

	/** Get Client.
	  * Client/Tenant for this installation.
	  */
	public int getAD_Client_ID();

    /** Column name AD_Org_ID */
    public static final String COLUMNNAME_AD_Org_ID = "AD_Org_ID";

	/** Set Organization.
	  * Organizational entity within client
	  */
	public void setAD_Org_ID (int AD_Org_ID);

	/** Get Organization.
	  * Organizational entity within client
	  */
	public int getAD_Org_ID();

    /** Column name C_FiscalPrintConf_ID */
    public static final String COLUMNNAME_C_FiscalPrintConf_ID = "C_FiscalPrintConf_ID";

	/** Set Fiscal Print Configuration	  */
	public void setC_FiscalPrintConf_ID (int C_FiscalPrintConf_ID);

	/** Get Fiscal Print Configuration	  */
	public int getC_FiscalPrintConf_ID();

    /** Column name C_FiscalPrintConf_UU */
    public static final String COLUMNNAME_C_FiscalPrintConf_UU = "C_FiscalPrintConf_UU";

	/** Set C_FiscalPrintConf_UU	  */
	public void setC_FiscalPrintConf_UU (String C_FiscalPrintConf_UU);

	/** Get C_FiscalPrintConf_UU	  */
	public String getC_FiscalPrintConf_UU();

    /** Column name Created */
    public static final String COLUMNNAME_Created = "Created";

	/** Get Created.
	  * Date this record was created
	  */
	public Timestamp getCreated();

    /** Column name CreatedBy */
    public static final String COLUMNNAME_CreatedBy = "CreatedBy";

	/** Get Created By.
	  * User who created this records
	  */
	public int getCreatedBy();

    /** Column name FiscalPrintModel */
    public static final String COLUMNNAME_FiscalPrintModel = "FiscalPrintModel";

	/** Set Fiscal Print Model	  */
	public void setFiscalPrintModel (String FiscalPrintModel);

	/** Get Fiscal Print Model	  */
	public String getFiscalPrintModel();

    /** Column name FiscalPrintSerial */
    public static final String COLUMNNAME_FiscalPrintSerial = "FiscalPrintSerial";

	/** Set Fiscal Print Serial	  */
	public void setFiscalPrintSerial (String FiscalPrintSerial);

	/** Get Fiscal Print Serial	  */
	public String getFiscalPrintSerial();

    /** Column name IsActive */
    public static final String COLUMNNAME_IsActive = "IsActive";

	/** Set Active.
	  * The record is active in the system
	  */
	public void setIsActive (boolean IsActive);

	/** Get Active.
	  * The record is active in the system
	  */
	public boolean isActive();

    /** Column name ModelName */
    public static final String COLUMNNAME_ModelName = "ModelName";

	/** Set ModelName	  */
	public void setModelName (String ModelName);

	/** Get ModelName	  */
	public String getModelName();

    /** Column name Name */
    public static final String COLUMNNAME_Name = "Name";

	/** Set Name.
	  * Alphanumeric identifier of the entity
	  */
	public void setName (String Name);

	/** Get Name.
	  * Alphanumeric identifier of the entity
	  */
	public String getName();

    /** Column name Port */
    public static final String COLUMNNAME_Port = "Port";

	/** Set Port	  */
	public void setPort (int Port);

	/** Get Port	  */
	public int getPort();

    /** Column name Updated */
    public static final String COLUMNNAME_Updated = "Updated";

	/** Get Updated.
	  * Date this record was updated
	  */
	public Timestamp getUpdated();

    /** Column name UpdatedBy */
    public static final String COLUMNNAME_UpdatedBy = "UpdatedBy";

	/** Get Updated By.
	  * User who updated this records
	  */
	public int getUpdatedBy();

    /** Column name URL */
    public static final String COLUMNNAME_URL = "URL";

	/** Set URL.
	  * Full URL address - e.g. http://www.idempiere.org
	  */
	public void setURL (String URL);

	/** Get URL.
	  * Full URL address - e.g. http://www.idempiere.org
	  */
	public String getURL();
}
