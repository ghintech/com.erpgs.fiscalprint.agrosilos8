package com.erpgs.fiscalprint.agrosilos.component;

import java.math.BigDecimal;

import org.adempiere.base.event.AbstractEventHandler;
import org.adempiere.base.event.IEventTopics;
import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.MInvoiceLine;
import org.compiere.model.MOrderLine;
import org.compiere.model.PO;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.osgi.service.event.Event;


public class ModelValidator extends AbstractEventHandler {
	private static CLogger log = CLogger.getCLogger(ModelValidator.class);
	
	@Override
	protected void doHandleEvent(Event event) {
		//System.out.println("opsaos");
		PO po = getPO(event);
		String type = event.getTopic();
		log.info(po.get_TableName() + " Type: " + type);
		if (po.get_TableName().equals(MOrderLine.Table_Name) ) {
			MOrderLine ol = (MOrderLine)po;
			if(type.equals(IEventTopics.PO_BEFORE_NEW)){
				//check if previous line has discount
				//log.warning("PRUEBA");
				if(((BigDecimal)ol.get_Value("PriceEntered")).compareTo(Env.ZERO)<0) {
					BigDecimal gt = DB.getSQLValueBD(null, "SELECT GrandTotal FROM C_Order WHERE C_Order_ID = ?",ol.getC_Order_ID());
					
					if(gt.compareTo((BigDecimal)ol.get_Value("PriceEntered"))<0)
						throw new AdempiereException("La linea debe ser un descuento global");
				}
			}
		}
		if (po.get_TableName().equals(MInvoiceLine.Table_Name) ) {
			MInvoiceLine ol = (MInvoiceLine)po;
			if(type.equals(IEventTopics.PO_BEFORE_NEW)){
				//check if previous line has discount
				//log.warning("PRUEBA");
				if(((BigDecimal)ol.get_Value("PriceEntered")).compareTo(Env.ZERO)<0) {
					BigDecimal gt = DB.getSQLValueBD(null, "SELECT GrandTotal FROM C_Invoice WHERE C_Order_ID = ?",ol.getC_Invoice_ID());
					
					if(gt.compareTo((BigDecimal)ol.get_Value("PriceEntered"))<0)
						throw new AdempiereException("La linea debe ser un descuento global");
				}
			}
		}
	}

	@Override
	protected void initialize() {
		//System.out.println("opsaos");
		registerTableEvent(IEventTopics.PO_BEFORE_NEW, MOrderLine.Table_Name);
		registerTableEvent(IEventTopics.PO_BEFORE_NEW, MInvoiceLine.Table_Name);
	}

}
