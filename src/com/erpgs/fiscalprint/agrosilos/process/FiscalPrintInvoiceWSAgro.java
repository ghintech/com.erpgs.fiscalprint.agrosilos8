package com.erpgs.fiscalprint.agrosilos.process;

import java.io.*;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.Normalizer;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.logging.Level;

import org.compiere.acct.DocManager;
import org.compiere.model.MAcctSchema;
import org.compiere.model.MBPartner;
import org.compiere.model.MBPartnerLocation;
import org.compiere.model.MInvoice;
import org.compiere.model.MInvoiceLine;
import org.compiere.model.MOrder;
import org.compiere.model.MPInstance;
import org.compiere.model.MProcess;
import org.compiere.model.Query;
import org.compiere.process.ProcessInfo;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;

import com.cds.sap.webservice.process.ReadWS;
import com.erpgs.fiscalprint.agrosilos.model.MFiscalPrintConf;
import com.erpgs.fiscalprint.agrosilos.model.MInvoiceFiscal;

public class FiscalPrintInvoiceWSAgro extends SvrProcess{
	private int p_C_Invoice_ID=0;
	private int p_C_InvoiceTo_ID=0;
	private int p_C_Doctype_ID=0;
	private int p_C_Order_ID=0;
	private int p_C_FiscalPrintConf_ID=0;
	private String p_ReportType=null;
	private Date p_Date1=null;
	private Date p_Date2=null;
	private MFiscalPrintConf fiscalprint=null;
	private String date1aux;
	private String date2aux;
	private Date p_DateInvoiced=null;
	private Date p_DateInvoiced2=null;
	private String p_XX_LoadOrder_Documentno="";
	private String p_mode="S";
	@Override
	protected void prepare() {
		// TODO Auto-generated method stub
		ProcessInfoParameter[] para = getParameter();
		for (int i = 0; i < para.length; i++)
		{
			String name = para[i].getParameterName();

			if (para[i].getParameter() == null)
				;
			else if (name.equals("C_FiscalPrintConf_ID"))
				p_C_FiscalPrintConf_ID =  para[i].getParameterAsInt();
			else if (name.equals("C_Invoice_ID")){
				p_C_Invoice_ID =  para[i].getParameterAsInt();
				p_C_InvoiceTo_ID = para[i].getParameter_ToAsInt();				
			}
			else if (name.equals("C_Order_ID"))
				p_C_Order_ID =  para[i].getParameterAsInt();
			else if (name.equals("C_DocType_ID"))
				p_C_Doctype_ID =  para[i].getParameterAsInt();
			else if (name.equals("ReportType"))
				p_ReportType =  para[i].getParameterAsString();
			else if (name.equals("Date")){
				p_Date1 =  (Timestamp)para[i].getParameter();
				p_Date2 =  (Timestamp)para[i].getParameter_To();

			}
			else if (name.equals("DateInvoiced")){
				p_DateInvoiced =  (Timestamp)para[i].getParameter();
				p_DateInvoiced2 =  (Timestamp)para[i].getParameter_To();

			}
			else if (name.equals("XX_LoadOrder_Documentno"))
				p_XX_LoadOrder_Documentno =  para[i].getParameterAsString();
			else
				log.log(Level.SEVERE, "Unknown Parameter: " + name);
		}
	}

	@Override
	protected String doIt() throws Exception {

		// TODO Auto-generated method stub
		fiscalprint=new MFiscalPrintConf(getCtx(), p_C_FiscalPrintConf_ID, get_TrxName());
		MInvoice invoice=null;
		MOrder order=null;
		List<MInvoice> invoices = null;
		boolean check=false;
		if(p_ReportType!=null){
			if(fiscalprint.getFiscalPrintModel().compareTo("H")==0)
				check=HasarZXReport(p_ReportType);
			else if(fiscalprint.getFiscalPrintModel().compareTo("S")==0)
				check=StarZXReport(p_ReportType);
			
			if(!check) return "No se ha podido imprimir. Existe un reporte en espera.";
		}else{
			if(p_C_InvoiceTo_ID !=0 && p_C_Invoice_ID!=0){
				invoices = new Query(getCtx(), MInvoice.Table_Name, "C_Doctype_ID = "+p_C_Doctype_ID+
						" AND DocStatus = 'CO' AND WasPrinted != 'Y' AND C_invoice_ID between "+p_C_Invoice_ID+
						" AND "+p_C_InvoiceTo_ID, get_TrxName()).setClient_ID().setOrderBy("DocumentNo").list();
			}
			else if(p_C_Invoice_ID!=0){
				invoice =new MInvoice(getCtx(), p_C_Invoice_ID, get_TrxName());			
			}
			else if(p_C_Order_ID!=0){
				order=new MOrder(getCtx(),p_C_Order_ID,get_TrxName());
				invoice =new MInvoice(getCtx(), order.getC_Invoice_ID(), get_TrxName());
				
				
			}
			else if (p_DateInvoiced != null && p_DateInvoiced2 != null) {
				invoices = new Query(getCtx(), MInvoice.Table_Name, "C_Doctype_ID = "+p_C_Doctype_ID+
						" AND DocStatus = 'CO' AND WasPrinted != 'Y' AND DateInvoiced between ? AND ?", get_TrxName()).setParameters(p_DateInvoiced,p_DateInvoiced2).setClient_ID().setOrderBy("DocumentNo").list();
				
			}
			if (p_XX_LoadOrder_Documentno.compareTo("")!=0) {
				
				invoices = new Query(getCtx(), MInvoice.Table_Name, "C_Doctype_ID = "+p_C_Doctype_ID+
						" AND DocStatus = 'CO' AND WasPrinted != 'Y' AND XX_LoadOrder_Documentno='"+p_XX_LoadOrder_Documentno+"'", get_TrxName()).setClient_ID().setOrderBy("DocumentNo").list();
				if (invoices==null) {
					this.statusUpdate("Consultando facturas en SAP espere un momento por favor...");
					ProcessInfoParameter p1 = new ProcessInfoParameter("CDS_SapWsRequest_ID",1000020,"", "", "");
					ProcessInfoParameter p2 = new ProcessInfoParameter("DocAction","CO","", "", "");
					ProcessInfoParameter p3 = new ProcessInfoParameter("DateTrx",Timestamp.valueOf(LocalDateTime.now().minusDays(1)),Timestamp.valueOf(LocalDateTime.now()), "", "");
					ProcessInfo pi = new ProcessInfo("ReadWS", 0, 0, 0);
					pi.setParameter(new ProcessInfoParameter[]{p1,p2,p3});
					MProcess process = new Query(getCtx(),MProcess.Table_Name,  "Value = ?", get_TrxName()).setParameters("ReadWS").first();

					MPInstance instance = new MPInstance(getCtx(),0,null);
					instance.setAD_Process_ID(process.get_ID() );
					instance.setRecord_ID(0);
					instance.saveEx();
					pi.setAD_PInstance_ID(instance.getAD_PInstance_ID());
					ReadWS rws = new ReadWS();
					boolean result = rws.startProcess(getCtx(), pi, null);
					if(!result) return "Ha ocurrido un problema leyendo la información desde SAP";
					this.statusUpdate("Facturas de SAP leidas exitosamente. Enviando a impresora fiscal");
					invoices = new Query(getCtx(), MInvoice.Table_Name, "C_Doctype_ID = "+p_C_Doctype_ID+
							" AND DocStatus = 'CO' AND WasPrinted != 'Y' AND XX_LoadOrder_Documentno='"+p_XX_LoadOrder_Documentno+"'", get_TrxName()).setClient_ID().setOrderBy("DocumentNo").list();
					
				}
			}
			//MDocType dt = new MDocType(getCtx(), invoice.getC_DocTypeTarget_ID(), get_TrxName());
			
			
			if(fiscalprint.getFiscalPrintModel().compareTo("H")==0)
				if(invoices!=null)
					check=HasarInvoice(invoices);
				else{
					check=HasarInvoice(invoice);
				}
			else if(fiscalprint.getFiscalPrintModel().compareTo("S")==0){
				if(invoices!=null)
					check=StarInvoice(invoices);
				else{
					//if(!invoice.get_ValueAsBoolean("WasPrinted"))
					check=StarInvoice(invoice);
				}

			}

		}

		if(invoices!=null)
			return "Cantidad de facturas: "+String.valueOf(invoices.size());
		
		return "Ok";
	}


	private boolean StarInvoice(MInvoice invoice) {
		/**
		 * 
		 * jSNOMBRE RAZON SOCIAL
			jRRUC_CEDULA
			j3LINEA 3 ENCABEZADO
			j4LINEA 4 ENCABEZADO
			j5LINEA 5 Encabezado
			j6LINEA 6
			@COMENTARIO
			!000000850000001000CHRISTIE`S 3825T/3826T TITANI+ME690
			p-2000
			!000000880000001000PROG. GENERICO
			p-2000
			!000000880000001000PROG. GENERICO
			p-2000
			@prueba de comentarios para que se muestr
			@ en la fiscal
			@  ***GARANTIA***
			101
		 * 
		 */

		//StringBuilder file=null;
		//String auxname=null;
		//String auxproductname = null;		
		try{
			fillWriter(invoice);


		}
		catch(Exception e){
			e.printStackTrace();
		}

		return true;
	}

	private boolean StarInvoice(List<MInvoice> invoices) throws IOException {
		

		//StringBuilder file = new StringBuilder("");
		for(MInvoice invoice:invoices){
			fillWriter(invoice);
		}
		return true;
	}
	private boolean HasarInvoice(List<MInvoice> invoices) throws IOException {
		
		for(MInvoice invoice:invoices){
			HasarInvoice(invoice);
		}
		return true;
	}
	public void fillWriter( MInvoice invoice) throws IOException{

		String linefeed="||";
		StringBuilder writer = new StringBuilder();
		MBPartner bpartner=new MBPartner(getCtx(), invoice.getC_BPartner_ID(), get_TrxName());
		MBPartnerLocation bplocation=new MBPartnerLocation(getCtx(),invoice.getC_BPartner_Location_ID(),get_TrxName());

		//verificamos si la factura ya ha sido impresa imprimimos un comprobante no fiscal
		boolean WasPrinted = false;
		
		WasPrinted = invoice.get_ValueAsBoolean("WasPrinted");
		String fNumber = invoice.get_ValueAsString("fiscalNumber");
		if(!fNumber.equals("")){
			writer.append("R");
			/*System.out.println(cmd);
            System.out.println(ticket.getTicketType());
            System.out.println(fNumber);*/
			if(invoice.getC_DocTypeTarget().getDocBaseType().compareTo("ARI")==0){
				writer.append("F");
			}else{
				writer.append("C");
			}
			writer.append(fNumber.substring((fNumber.length()-7),fNumber.length()));
			writer.append(fNumber.substring((fNumber.length()-7),fNumber.length()));
			invoice.set_ValueOfColumn("Script", writer.toString());
			invoice.set_ValueOfColumn("WasPrinted", false);
			invoice.set_ValueOfColumn("C_FiscalPrintConf_ID", p_C_FiscalPrintConf_ID);
			invoice.saveEx();
			return;
		}            

		if(invoice.getDateInvoiced()!=new Timestamp(System.currentTimeMillis())){
			invoice.setDateInvoiced(new Timestamp(System.currentTimeMillis()));
			invoice.setDateAcct(new Timestamp(System.currentTimeMillis()));
			invoice.saveEx();
			DocManager.postDocument(MAcctSchema.getClientAcctSchema(getCtx(), getAD_Client_ID()), MInvoice.Table_ID, invoice.getC_Invoice_ID(), true, true, get_TrxName());
		}
		invoice.saveEx();
		String Address=" ";
		if(bplocation.getC_Location().getAddress1()!=null)
			Address=bplocation.getC_Location().getAddress1().trim();
		if(bplocation.getC_Location().getAddress2()!=null)
			Address+=" "+bplocation.getC_Location().getAddress2().trim();
		if(bplocation.getC_Location().getAddress3()!=null)
			Address+=" "+bplocation.getC_Location().getAddress3().trim();
		if(bplocation.getC_Location().getAddress4()!=null)
			Address+=" "+bplocation.getC_Location().getAddress4().trim();
		int length=100;

		if(Address.length()<=100){
			length=Address.length();
		}
		int j=1;
		if(invoice.getC_DocTypeTarget().getDocBaseType().compareTo("ARI")==0){			    
			writer.append("j"+String.valueOf(j++)+"RUC/CIP: "+((bpartner.getTaxID()==null)?"0":bpartner.getTaxID()));
			if(bpartner.get_Value("dv")!=null) {
				writer.append(" DV " +bpartner.get_Value("dv"));
			}
			writer.append(linefeed);
			writer.append("j"+String.valueOf(j++)+"RAZON SOCIAL: "+ linefeed);
			if(bpartner.getName().length()>45) {
				writer.append("j"+String.valueOf(j++) +stripAccents(bpartner.getName().replaceAll("&", "").substring(0, 44)+ linefeed));
				writer.append("j"+String.valueOf(j++) +stripAccents(bpartner.getName().replaceAll("&", "").substring(45, bpartner.getName().replaceAll("&", "").length())+ linefeed));
			}else
				writer.append("j"+String.valueOf(j++)+stripAccents(bpartner.getName().replaceAll("&", "").substring(0, bpartner.getName().replaceAll("&", "").length())+ linefeed));

			if(bplocation.getC_Location().getAddress1()!=null)
				writer.append("j"+String.valueOf(j++) +stripAccents(bplocation.getC_Location().getAddress1()) + linefeed);
				//writer.append("j"+String.valueOf(j++) +bplocation.getC_Location().getAddress1().replaceAll("[^A-Za-z0-9 ]", "") + linefeed);
			
			if(bplocation.getC_Location().getAddress2()!=null)
				writer.append("j"+String.valueOf(j++) +stripAccents(bplocation.getC_Location().getAddress2()) + linefeed);
				//writer.append("j"+String.valueOf(j++) +bplocation.getC_Location().getAddress2().replaceAll("[^A-Za-z0-9 ]", "") + linefeed);
			if(bplocation.getC_Location().getAddress3()!=null)
				writer.append("j"+String.valueOf(j++) +stripAccents(bplocation.getC_Location().getAddress3()) + linefeed);
				//writer.append("j"+String.valueOf(j++) +bplocation.getC_Location().getAddress3().replaceAll("[^A-Za-z0-9 ]", "") + linefeed);


			writer.append("j"+String.valueOf(j++) +invoice.getDocumentNo() + linefeed);
			
		}else{
			writer.append("jS" + stripAccents(bpartner.getName()) + linefeed);
			writer.append("jR"+((bpartner.getTaxID()==null)?"0":bpartner.getTaxID()) + linefeed);

			writer.append("j3"+stripAccents(Address.substring(0, length)) + linefeed);
			writer.append("jF"+invoice.get_Value("fiscalNumber") + linefeed);
			writer.append("j01"+invoice.getDocumentNo() + linefeed);


		}
		List<MInvoiceLine> invoicelinequery= new Query(getCtx(), MInvoiceLine.Table_Name, 
				MInvoiceLine.COLUMNNAME_C_Invoice_ID+"=?", get_TrxName()).setParameters(invoice.getC_Invoice_ID())
				.setOrderBy(MInvoiceLine.COLUMNNAME_Line)
				.list();


		for (MInvoiceLine invoiceline:invoicelinequery) {
			String itemprefix=" ";
			String linedescription = " ";
			if(invoiceline.getM_Product().getName()!=null)
				//auxproductname = invoiceline.getM_Product().getName().replace("&amp;", "&").replace("&quot;", "\"").replace("&apos;", "\'");
				if(invoiceline.getM_Product_ID()!=0)
					linedescription= invoiceline.getM_Product().getName().substring(0, Math.min(invoiceline.getM_Product().getName().length(), 35));
				else if(invoiceline.getC_Charge_ID()!=0)
					linedescription= invoiceline.getC_Charge().getName().substring(0, Math.min(invoiceline.getC_Charge().getName().length(), 35));
			if(invoiceline.getDescription()!=null)
				linedescription+=" "+invoiceline.getDescription().substring(0, Math.min(invoiceline.getDescription().length(), 60));
			if(invoice.getC_DocTypeTarget().getDocBaseType().compareTo("ARI")==0){
				//log.log(Level.SEVERE, invoiceline.getC_Tax().getRate().toString());
				if(invoiceline.getC_Tax().getRate().compareTo(new BigDecimal(7))==0)
					itemprefix="!";
				else if (invoiceline.getC_Tax().getRate().compareTo(new BigDecimal(10))==0)
					itemprefix=String.valueOf((char)34);
				else if (invoiceline.getC_Tax().getRate().compareTo(new BigDecimal(15))==0)
					itemprefix=String.valueOf((char)35);
				if(invoiceline.getLineNetAmt().compareTo(BigDecimal.ZERO)>=0) {	
					//log.log(Level.SEVERE, itemprefix.toString());	    		
					//writer.append("@Codigo: "+ invoiceline.getM_Product().getValue()+linefeed);
					writer.append(itemprefix + String.format(Locale.ENGLISH,"%011.4f", invoiceline.getPriceEntered()).replace(".", "") 
							+ String.format(Locale.ENGLISH,"%09.3f",invoiceline.getQtyEntered()).replace(".", "") + stripAccents(linedescription)  + linefeed);
				}else {
					log.warning(invoiceline.getLine()+"+");
					if(invoiceline.get_ValueAsBoolean("IsGlobalDiscount")) {
						log.warning(invoiceline.getLine()+"++");
						writer.append("3"+ linefeed);
					}
					log.warning(writer.toString()+"+++");
					writer.append("q-"+ String.format(Locale.ENGLISH,"%010.2f", invoiceline.getLineNetAmt()).replace(".", "") +linefeed);					
				}
			}else{
				itemprefix="d0";
				if(invoiceline.getC_Tax().getRate().compareTo(new BigDecimal(7))==0)
					itemprefix="d1";
				else if (invoiceline.getC_Tax().getRate().compareTo(new BigDecimal(10))==0)
					itemprefix="d2";
				else if (invoiceline.getC_Tax().getRate().compareTo(new BigDecimal(15))==0)
					itemprefix="d3";
				//writer.append("@Codigo: "+ invoiceline.getM_Product().getValue()+linefeed);
				/*if(invoiceline.getM_Product().getName()!=null)
					//auxproductname = invoiceline.getM_Product().getName().replace("&amp;", "&").replace("&quot;", "\"").replace("&apos;", "\'");
					if(invoiceline.getM_Product_ID()!=0)
						linedescription= invoiceline.getM_Product().getName().substring(0, Math.min(invoiceline.getM_Product().getName().length(), 25));
					else if(invoiceline.getC_Charge_ID()!=0)
						linedescription= invoiceline.getC_Charge().getName().substring(0, Math.min(invoiceline.getC_Charge().getName().length(), 25));
				if(invoiceline.getDescription()!=null) {
					//linedescription+=invoiceline.getDescription().substring(0, Math.min(invoiceline.getDescription().length(), 45));
					linedescription+=invoiceline.getDescription().trim();
				}*/
				if(invoiceline.getLineNetAmt().compareTo(BigDecimal.ZERO)>=0) {
					if(linedescription.length()>25)
						linedescription.substring(0,25);
					writer.append(itemprefix 
							+ String.format(Locale.ENGLISH,"%011.4f", invoiceline.getPriceEntered()).replace(".", "") 
							+ String.format(Locale.ENGLISH,"%09.3f",invoiceline.getQtyEntered()).replace(".", "") 
							+ stripAccents(linedescription)  
							+ linefeed);
				}else {
					log.warning(writer.toString()+"+");
					if(invoiceline.get_ValueAsBoolean("IsGlobalDiscount")) {
						writer.append("3"+ linefeed);
						log.warning(writer.toString()+"+++");
					}
					writer.append("q-"+ String.format(Locale.ENGLISH,"%010.2f", invoiceline.getLineNetAmt()).replace(".", "") +linefeed);					
				}				
			}			    					    

		}
		writer.append("101");
		invoice.set_ValueOfColumn("Script", writer.toString());
		invoice.set_ValueOfColumn("C_FiscalPrintConf_ID", p_C_FiscalPrintConf_ID);

		//invoice.setProcessed(true);

		invoice.saveEx();
		/*MFiscalReportLog reportLog=new MFiscalReportLog(getCtx(), 0, get_TrxName());
	    reportLog.setAD_Org_ID(invoice.getAD_Org_ID());
	    reportLog.setC_Invoice_ID(invoice.get_ID());
	    reportLog.setScript(writer.toString());
	    reportLog.saveEx();*/

	}

	public boolean HasarInvoice(MInvoice invoice) throws FileNotFoundException{
		File file=null;
		//Hasar objHasar =new Hasar();
		try{
			//file = new File("factura.txt");    
			//if(file.exists())
			//file.delete();
			//file.createNewFile();
			// creates a FileWriter Object
			//FileWriter writer = new FileWriter(file);
			StringBuilder writer = new StringBuilder();
			// Writes the content to the file

			/*MBPartner bpartner=new MBPartner(getCtx(), invoice.getC_BPartner_ID(), get_TrxName());

			char TD=objHasar.TD;
			if(invoice.getC_DocTypeTarget().getDocBaseType().compareTo("ARC")==0){


				TD='D';
				writer.append(objHasar.DF).append(objHasar.FS).append(bpartner.getName() + objHasar.FS + ((bpartner.getTaxID()==null)?"0":bpartner.getTaxID()))
				.append(objHasar.FS).append("COD:"+ bpartner.getValue().trim()).append(objHasar.FS).append(objHasar.NR).append(objHasar.FS).append(objHasar.FS).append(objHasar.FS)
				.append(TD).append(objHasar.LF);	
			}else{
				writer.append(objHasar.DF).append(objHasar.FS).append(bpartner.getName() + objHasar.FS + ((bpartner.getTaxID()==null)?"0":bpartner.getTaxID()))
				.append(objHasar.FS).append("COD:"+ bpartner.getValue().trim()).append(objHasar.FS).append(objHasar.NR).append(objHasar.FS)
				.append(TD).append(objHasar.LF);
			}
			MBPartnerLocation bplocation=new MBPartnerLocation(getCtx(),invoice.getC_BPartner_Location_ID(),get_TrxName());
			String Address=" ";
			if(bplocation.getC_Location().getAddress1()!=null)
				Address=bplocation.getC_Location().getAddress1().trim();
			if(bplocation.getC_Location().getAddress2()!=null)
				Address+=" "+bplocation.getC_Location().getAddress2().trim();
			if(bplocation.getC_Location().getAddress3()!=null)
				Address+=" "+bplocation.getC_Location().getAddress3().trim();
			if(bplocation.getC_Location().getAddress4()!=null)
				Address+=" "+bplocation.getC_Location().getAddress4().trim();
			if(Address.length()<=100){
			}
			//writer.append(objHasar.CED).append(objHasar.FS).append('1').append(objHasar.FS).append("Dir:"+Address.substring(0, length)).append(objHasar.LF);	

			List<MInvoiceLine> invoicelinequery= new Query(getCtx(), MInvoiceLine.Table_Name, 
					MInvoiceLine.COLUMNNAME_C_Invoice_ID+"=?", get_TrxName()).setParameters(invoice.getC_Invoice_ID()).list();

			NumberFormat f = NumberFormat.getInstance(Locale.ENGLISH);
			if (f instanceof DecimalFormat) {
				((DecimalFormat) f).setDecimalSeparatorAlwaysShown(true);
			}
			String aux="";
			for (MInvoiceLine invoiceline:invoicelinequery) {
				writer.append(objHasar.PI).append(objHasar.FS).append(((invoiceline.getM_Product().getName()!=null)?
						invoiceline.getM_Product().getName().subSequence(0,invoiceline.getM_Product().getName().length()):
							" ")).append(objHasar.FS);
				aux=f.format(invoiceline.getQtyEntered());
				writer.append(aux.subSequence(0, aux.length())).append(objHasar.FS);
				aux=f.format(invoiceline.getPriceEntered());
				writer.append(aux.subSequence(0, aux.length())).append(objHasar.FS);
				aux=f.format(invoiceline.getC_Tax().getRate());
				writer.append(aux.subSequence(0, aux.length())).append(objHasar.FS);
				writer.append('M').append(objHasar.FS)
				.append(((invoiceline.getM_Product().getValue()!=null)?
						invoiceline.getM_Product().getValue().subSequence(0,invoiceline.getM_Product().getValue().length()): " ")).append(objHasar.LF);
			}




			writer.append('E').append(objHasar.LF);**/
			//invoice.set_ValueOfColumn("Script", URLEncoder.encode(writer.toString(), "UTF-8"));
			if(invoice.getDateInvoiced()!=new Timestamp(System.currentTimeMillis())){
				invoice.setDateInvoiced(new Timestamp(System.currentTimeMillis()));
				invoice.setDateAcct(new Timestamp(System.currentTimeMillis()));
				invoice.saveEx();
				DocManager.postDocument(MAcctSchema.getClientAcctSchema(getCtx(), getAD_Client_ID()), MInvoice.Table_ID, invoice.getC_Invoice_ID(), true, true, get_TrxName());
			}
			invoice.set_ValueOfColumn("Script", "--");
			invoice.set_ValueOfColumn("C_FiscalPrintConf_ID", p_C_FiscalPrintConf_ID);

			invoice.saveEx();
			//writer.flush();
			//writer.close();

		}

		catch(Exception e){
			e.printStackTrace();
		}

		return true;




	}

	public boolean HasarZXReport(String ReportType){
		if(p_DateInvoiced!=null){
			Calendar date1=Calendar.getInstance();
			date1.setTimeInMillis(p_DateInvoiced.getTime());
			date1aux=String.format("%4d",date1.get(Calendar.YEAR)).substring(2, 4) +
					String.format("%02d",date1.get(Calendar.MONTH)+1) +
					String.format( "%02d",date1.get(Calendar.DAY_OF_MONTH));
			Calendar date2=Calendar.getInstance();
			date2.setTimeInMillis(p_DateInvoiced2.getTime());
			date2aux=String.format("%4d",date2.get(Calendar.YEAR)).substring(2, 4) +
					String.format("%02d",date2.get(Calendar.MONTH)+1) +
					String.format( "%02d",date2.get(Calendar.DAY_OF_MONTH));

			System.out.println(date1aux);
			System.out.println(date2aux);
			//return false;
		}
		
		fiscalprint=new MFiscalPrintConf(getCtx(), p_C_FiscalPrintConf_ID, get_TrxName());
		MInvoiceFiscal reportpending=new Query(getCtx(),MInvoiceFiscal.Table_Name,"C_FiscalPrintConf_ID=? AND AD_Org_ID=? AND (Script ='9|Z' OR Script ='9|X') AND WasPrinted='N'",get_TrxName()).setClient_ID()
				.setParameters(p_C_FiscalPrintConf_ID,fiscalprint.getAD_Org_ID()).first();
		if(reportpending!=null) {
			return false;
		}
		MInvoiceFiscal fiscalinfo = new MInvoiceFiscal(getCtx(), 0, get_TrxName());
		fiscalinfo.setAD_Org_ID(fiscalprint.getAD_Org_ID());
		fiscalinfo.set_ValueOfColumn("C_FiscalPrintConf_ID", p_C_FiscalPrintConf_ID);
	    if(ReportType.compareTo("Z")==0){
	    	fiscalinfo.set_ValueOfColumn("Script","9|Z");
	    }else if (ReportType.compareTo("X")==0){
	    	fiscalinfo.set_ValueOfColumn("Script","9|X");
	    }else {
	    	log.warning(":|"+date1aux+"|"+date2aux+"|G");
	    	fiscalinfo.set_ValueOfColumn("Script",":|"+date1aux+"|"+date2aux+"|G");
	    }
	    fiscalinfo.saveEx();
	    return true;

	}

	private boolean StarZXReport(String ReportType) {
		if(p_DateInvoiced!=null){
			Calendar date1=Calendar.getInstance();
			date1.setTimeInMillis(p_DateInvoiced.getTime());
			date1aux=String.format( "%02d",date1.get(Calendar.DAY_OF_MONTH)) +
					String.format("%02d",date1.get(Calendar.MONTH)+1) +
					String.format("%4d",date1.get(Calendar.YEAR)).substring(2, 4);

			Calendar date2=Calendar.getInstance();
			date2.setTimeInMillis(p_DateInvoiced2.getTime());
			date2aux=String.format( "%02d",date2.get(Calendar.DAY_OF_MONTH)) +
					String.format("%02d",date2.get(Calendar.MONTH)+1) +	
					String.format("%4d",date2.get(Calendar.YEAR)).substring(2, 4) ;

			System.out.println(date1aux);
			System.out.println(date2aux);
			//return "";
		}
		/*StringBuilder writer=null;

		try{
			writer = new StringBuilder("factura.txt");    
			//if(file.exists())
			//	file.delete();
			//file.createNewFile();
			// creates a FileWriter Object
			//FileWriter writer = new FileWriter(file); 
			// Writes the content to the file
			if(ReportType.compareTo("Z")==0){
				writer.append("I0Z");
			}else if(ReportType.compareTo("X")==0){
				writer.append("I0X");
			}else{
				writer.append("I2S" + date1aux + date2aux);
			}
		 	
			//writer.flush();
			//writer.close();
			/*MFiscalReportLog reportLog=new MFiscalReportLog(getCtx(), 0, get_TrxName());
		    reportLog.setAD_Org_ID(0);

		    reportLog.setScript(writer.toString());
		    reportLog.saveEx();*/
		/*}

		catch(Exception e){
			e.printStackTrace();
		}
		return true;*/
		fiscalprint=new MFiscalPrintConf(getCtx(), p_C_FiscalPrintConf_ID, get_TrxName());
		MInvoiceFiscal reportpending=new Query(getCtx(),MInvoiceFiscal.Table_Name,"C_FiscalPrintConf_ID=? AND AD_Org_ID=? AND (Script ='9|Z' OR Script ='9|X') AND WasPrinted='N'",get_TrxName()).setClient_ID()
				.setParameters(p_C_FiscalPrintConf_ID,fiscalprint.getAD_Org_ID()).first();
		if(reportpending!=null) {
			return false;
		}
		MInvoiceFiscal fiscalinfo = new MInvoiceFiscal(getCtx(), 0, get_TrxName());
		fiscalinfo.setAD_Org_ID(fiscalprint.getAD_Org_ID());
		fiscalinfo.set_ValueOfColumn("C_FiscalPrintConf_ID", p_C_FiscalPrintConf_ID);
	    if(ReportType.compareTo("Z")==0){
	    	fiscalinfo.set_ValueOfColumn("Script","I0Z");
	    } else if (ReportType.compareTo("X")==0){
	    	fiscalinfo.set_ValueOfColumn("Script","I0X");
	    }else{
	    	log.warning("I2"+p_mode+date1aux+date2aux);
	    	fiscalinfo.set_ValueOfColumn("Script","I2"+p_mode+date1aux+date2aux);
	    	//fiscalinfo.set_ValueOfColumn("Script","I1Z");
	    }
	    fiscalinfo.saveEx();
	    return true;
	}

	public String stripAccents(String s) 
	{
		if(s!=null) {
		    s = Normalizer.normalize(s, Normalizer.Form.NFD);
		    s = s.replaceAll("[\\p{InCombiningDiacriticalMarks}]", "");
		}
	    return s;
	}

}
